# full stack application deployement with kubernetes

Deploy a secure application in k8s
The application is the TodoMVC
## frontend React 
https://todomvc.com/
## backend Flask  
https://todobackend.com/
## TODO:

- Containerize all applications and deploy them using https://k3s.io/
- All api endpoints should be exposed to the public except the DELETE
- Delete should only be allowed by localhost and 35.246.169.21
- Add a load balancer that enables both backends to be used.
 Build a pipeline for gitlab that:
- runs the tests
- doesn’t leak any sensitive information

## deployment
- the application deployment is maintained by K3S project 
- a permanant storage of 1 Gb is allocated for the project data
- the backend is writtend in Flask and presents two replicas
- the frontend is written in React and presents one replica
