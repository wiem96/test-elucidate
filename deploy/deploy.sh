#!/bin/bash
# Install k3s
curl -sfL https://get.k3s.io | K3S_KUBECONFIG_MODE="644" sh -s -

# Check if response the node with the current version
k3s kubectl get node

# Create the namespace (only dev)
kubectl create namespace test-Elucidate

# Create ingress, service, and deployment
k3s kubectl apply -f frontend-deployment.yml
k3s kubectl apply -f backend-deployment.yml
k3s kubectl apply -f db-deployment.yml
k3s kubectl apply -f db-persistant-volume-claim.yml
k3s kubectl apply -f postgres-cluster-ip-service.yml
k3s kubectl apply -f api-cluster-ip-service.yml
k3s kubectl apply -f ingress-service.yml
k3s kubectl apply -f loadbalancer.yml

# Check ingress, service and pods
k3s kubectl get ingress,svc,pods -n test-Elucidate


